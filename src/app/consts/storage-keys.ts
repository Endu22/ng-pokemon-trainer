//Keys for our local and session storage
export const storageKey = 'pokemon-trainer';
export const pokemonKey = 'pokemon-data';
//Components for fetching images.
export const pictureUrl =
  'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';
export const pictureUrl_end = '.png';
