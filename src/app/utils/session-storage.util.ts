export class sessionStorageUtil {
  static saveToSessionStorage<T>(key: string, value: T): void {
    sessionStorage.setItem(key, JSON.stringify(value));
  }

  static getFromSessionStorage<T>(key: string): T | undefined {
    const dataInStorage = sessionStorage.getItem(key);

    try {
      if (dataInStorage) {
        return JSON.parse(dataInStorage) as T;
      }
      return undefined;
    } catch (e) {
      sessionStorage.removeItem(key);
      return undefined;
    }
  }
}

export class LocalStorageUtil {
  static getLocalStorage<T>(key: string): T | undefined {
    const dataInLocalStorage = localStorage.getItem(key);

    try {
      if (dataInLocalStorage) {
        return JSON.parse(dataInLocalStorage) as T;
      }
      return undefined;
    } catch (e) {
      localStorage.removeItem(key);
      return undefined;
    }
  }
  static saveToLocalStorage<T>(key: string, value: T): void {
    localStorage.setItem(key, JSON.stringify(value));
  }
}

export class removeUserSession {
  static logoutUser(key: string): void {
    sessionStorage.removeItem(key);
  }
}
