import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchService } from 'src/app/services/catch.service';

@Component({
  selector: 'app-pokemon-item-button',
  templateUrl: './pokemon-item-button.component.html',
  styleUrls: ['./pokemon-item-button.component.css'],
})
export class PokemonItemButtonComponent implements OnInit {
  //Public variables.
  loading: boolean = false;
  isCaught: boolean = false;
  isPokemonCatalogue: boolean = false;

  @Input() pokemon!: Pokemon;
  @Input() pokemonName!: string;
  @Input() isTrainer?: boolean;

  constructor(
    private catchService: CatchService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    //Choose input variable based on condition.
    if (this.isTrainer)
      this.isCaught = this.catchService.isCaught(this.pokemonName);
    else this.isCaught = this.catchService.isCaught(this.pokemon.name);
    //Used in HTML to disable button if true.
    if (this.router.url === '/pokemon-catalogue') {
      this.isPokemonCatalogue = true;
    }
  }

  onCatchClick(): void {
    this.loading = true;

    if (this.isCaught === false) {
      //Add to caught in session storage and api
      this.catchService.addToCaught(this.pokemon.name).subscribe({
        next: (trainer: Trainer) => {
          this.loading = false;
          //Update state isCaught to true/false.
          this.isCaught = this.catchService.isCaught(this.pokemon.name);
        },
        error: (error: HttpErrorResponse) => {
          console.log(error.message);
        },
      });
      this.isCaught = true;
    }
  }

  onCaughtClick(): void {
    this.loading = true;

    if (this.isCaught === true) {
      //remove from session storage and api
      this.catchService.removeFromCaught(this.pokemonName).subscribe({
        next: (trainer: Trainer) => {
          this.loading = false;
          this.isCaught = this.catchService.isCaught(this.pokemonName);
        },
        error: (error: HttpErrorResponse) => {
          console.log(error.message);
        },
      });
      this.isCaught = false;
    }
  }
}
