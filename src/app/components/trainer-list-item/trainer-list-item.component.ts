import { Component, Input, OnInit, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PictureService } from 'src/app/services/picture.service';

@Component({
  selector: 'app-trainer-list-item',
  templateUrl: './trainer-list-item.component.html',
  styleUrls: ['./trainer-list-item.component.css'],
})
export class TrainerListItemComponent implements OnInit {
  //Class variables
  private _pokeUrl: string = '';
  private _id: string = '';
  //Public variable
  trainer = true;

  @Input() pokemon: any;

  constructor(private readonly pictureService: PictureService) {}
  //Getters & Setters
  get getPicUrl(): string {
    return this.pictureService.getPictureUrl(this._id);
  }

  get getPokemonUrl(): string {
    return this.pictureService.getPokemonUrl(this.pokemon);
  }

  ngOnInit(): void {
    //Get pokemon url from input pokemon and id from pokemon url.
    this._pokeUrl = this.pictureService.getPokemonUrl(this.pokemon);
    this._id = this.pictureService.getPokemonImageId(this._pokeUrl);
  }
}
