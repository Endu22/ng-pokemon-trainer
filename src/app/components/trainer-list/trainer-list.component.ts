import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-trainer-list',
  templateUrl: './trainer-list.component.html',
  styleUrls: ['./trainer-list.component.css'],
})
export class TrainerListComponent implements OnInit {
  @Input() pokemons: any;

  constructor() {}

  ngOnInit(): void {}
}
