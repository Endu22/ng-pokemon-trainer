import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { storageKey } from 'src/app/consts/storage-keys';
import { TrainerService } from 'src/app/services/trainer.service';
import {
  removeUserSession,
  sessionStorageUtil,
} from 'src/app/utils/session-storage.util';

@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.css'],
})
export class LogoutButtonComponent implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly trainerService: TrainerService
  ) {}

  ngOnInit(): void {}

  logoutUser() {
    //Remove user session.
    removeUserSession.logoutUser(storageKey);
    this.trainerService.trainer =
      sessionStorageUtil.getFromSessionStorage(storageKey);
    //Redirect user to login.
    this.router.navigateByUrl('/login');
  }
}
