import { ShowMoreService } from './../../services/show-more.service';

import { Component, Input } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-show-more',
  templateUrl: './show-more.component.html',
  styleUrls: ['./show-more.component.css'],
})
export class ShowMoreComponent {
  @Input() pokemon?: Pokemon;
  //public variables.
  pokemonExtraData: [] = [];
  rerender: boolean = false;

  constructor(private readonly showMoreService: ShowMoreService) {}
  //Getters & Setters
  get pokemonData(): any {
    return this.pokemonExtraData;
  }

  set pokemonData(pokemonData) {
    this.pokemonExtraData = pokemonData;
  }

  //Function to show more information about pokemon. only get data if rerender is false.
  getMoreInfo(pokemon: Pokemon | undefined) {
    if (this.rerender === false) {
      this.showMoreService
        .getPokemonUrl(pokemon!)
        .subscribe((pokemonData) => (this.pokemonData = pokemonData));
    }
    this.rerender = !this.rerender;
  }
}
