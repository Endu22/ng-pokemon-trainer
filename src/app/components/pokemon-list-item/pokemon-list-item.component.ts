import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PictureService } from 'src/app/services/picture.service';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css'],
})
export class PokemonListItemComponent implements OnInit {
  private _id: string = '';

  @Input() pokemon?: Pokemon;

  constructor(private readonly pictureService: PictureService) {}

  ngOnInit(): void {
    //Get id from pokemon URL.
    this._id = this.pictureService.getPokemonImageId(this.pokemon!.url);
  }

  //Get id from pokemon URL.
  get getPicUrl(): string {
    return this.pictureService.getPictureUrl(this._id);
  }
}
