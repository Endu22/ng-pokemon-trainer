import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root',
})
export class PokemonCatalogueService {
  private _pokemons: Pokemon[] = [];

  //Getters & Setters.
  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  set pokemons(pokemons: Pokemon[]) {
    this._pokemons = pokemons;
  }

  constructor() {}
}
