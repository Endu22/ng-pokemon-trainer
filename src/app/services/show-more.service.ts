import { pokemonKey } from 'src/app/consts/storage-keys';
import { LocalStorageUtil } from 'src/app/utils/session-storage.util';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { extraPokeData, Pokemon } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root',
})
export class ShowMoreService {
  //Getting cached API data from localStorage.
  pokemonInStorage = LocalStorageUtil.getLocalStorage(pokemonKey);
  constructor(private readonly http: HttpClient) {}

  //Fetching 'show more' data.
  getPokemonUrl(pokemon: Pokemon) {
    return this.http.get<extraPokeData>(pokemon.url);
  }
}
