import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { pictureUrl, pokemonKey } from '../consts/storage-keys';
import { Pokemon } from '../models/pokemon.model';
import { LocalStorageUtil } from '../utils/session-storage.util';

@Injectable({
  providedIn: 'root',
})
export class PictureService {
  constructor(private readonly http: HttpClient) {}

  //Constructing picture URL.
  getPictureUrl(id: string): string {
    return `${pictureUrl}${id}.png`;
  }

  //Getting pokemon url based on name.
  getPokemonUrl(name: string): string {
    let pokemonUrl = '';
    LocalStorageUtil.getLocalStorage<Pokemon[]>(pokemonKey)?.forEach(
      (pokemon) => {
        if (pokemon.name === name) pokemonUrl = pokemon.url;
      }
    );

    return pokemonUrl;
  }
  //extract id from pokemon url.
  getPokemonImageId(url: string): string {
    const urlArr = url.split(/[/.]/);
    return urlArr[urlArr.length - 2];
  }
}
