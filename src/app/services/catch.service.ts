import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { storageKey } from '../consts/storage-keys';
import { headers } from '../headers/http-headers';
import { Trainer } from '../models/trainer.model';
import { sessionStorageUtil } from '../utils/session-storage.util';
import { TrainerService } from './trainer.service';

@Injectable({
  providedIn: 'root',
})
export class CatchService {
  constructor(
    private http: HttpClient,
    private readonly trainerService: TrainerService
  ) {}

  //Updating trainer in sessionstorage and patching API with the new trainer data.
  addToCaught(pokemonName: string): Observable<Trainer> {
    if (!this.trainerService.trainer) {
      throw new Error('addToCatched: There is no trainer');
    }

    const { apiTrainerUrl } = environment;

    const trainer: Trainer = this.trainerService.trainer;

    trainer.pokemon.push(pokemonName);

    sessionStorageUtil.saveToSessionStorage(storageKey, trainer);

    return this.http.patch<Trainer>(
      `${apiTrainerUrl}/${trainer.id}`,
      {
        pokemon: [...trainer.pokemon],
      },
      {
        headers,
      }
    );
  }

  //Check if caught pokemon contain argument pokemonName
  isCaught(pokemonName: string): boolean {
    return Boolean(
      this.trainerService.trainer!.pokemon.find(
        (pokemon: string) => pokemon === pokemonName
      )
    );
  }

  //remove pokemon from storage and patch API.
  removeFromCaught(pokemonName: string): Observable<Trainer> {
    if (!this.trainerService.trainer) {
      throw new Error('addToCatched: There is no trainer');
    }

    const { apiTrainerUrl } = environment;

    const trainer: Trainer = this.trainerService.trainer;

    trainer.pokemon = trainer.pokemon.filter(
      (pokemon: string) => pokemon !== pokemonName
    );

    sessionStorageUtil.saveToSessionStorage(storageKey, trainer);

    return this.http.patch<Trainer>(
      `${apiTrainerUrl}/${trainer.id}`,
      {
        pokemon: [...trainer.pokemon],
      },
      {
        headers,
      }
    );
  }
}
