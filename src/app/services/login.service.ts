import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { Trainer } from '../models/trainer.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { headers } from '../headers/http-headers';

const { apiTrainerUrl } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private readonly http: HttpClient) {}

  //checking if username exists. if not create a user.
  login(username: string): Observable<Trainer> {
    return this.checkIfUsernameExist(username).pipe(
      switchMap((trainer: Trainer | undefined) => {
        if (trainer === undefined) {
          return this.createTrainer(username);
        }

        return of(trainer);
      })
    );
  }

  //Fetching from trainer API. return trainer object.
  private checkIfUsernameExist(
    username: string
  ): Observable<Trainer | undefined> {
    return this.http
      .get<Trainer[]>(`${apiTrainerUrl}?username=${username}`)
      .pipe(map((response: Trainer[]) => response.pop()));
  }

  //Create a new user and post to trainer API.
  createTrainer(username: string): Observable<Trainer> {
    const trainer = {
      username,
      pokemon: [],
    };

    return this.http.post<Trainer>(apiTrainerUrl, trainer, { headers });
  }
}
