import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { pokemonKey } from '../consts/storage-keys';
import { Pokemon, pokemonNameAndUrl } from '../models/pokemon.model';
import { LocalStorageUtil } from '../utils/session-storage.util';

const { pokemonUrl } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  private _pokemon?: Pokemon;

  //Getters & setters
  get pokemon(): Pokemon | undefined {
    return this._pokemon;
  }

  //Saving the fetched API data to localStorage.
  set pokemon(pokemon: Pokemon | undefined) {
    LocalStorageUtil.saveToLocalStorage<Pokemon>(pokemonKey, pokemon!);
    this._pokemon = pokemon;
  }

  constructor(private readonly http: HttpClient) {}
  //Fetching pokemons from API.
  getPokemon() {
    return this.http.get<pokemonNameAndUrl>(`${pokemonUrl}`);
  }
}
