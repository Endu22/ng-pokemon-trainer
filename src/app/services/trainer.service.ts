import { Injectable } from '@angular/core';
import { storageKey } from '../consts/storage-keys';
import { Trainer } from '../models/trainer.model';
import { sessionStorageUtil } from '../utils/session-storage.util';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _trainer?: Trainer;

  //Set & getters
  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  set trainer(trainer: Trainer | undefined) {
    sessionStorageUtil.saveToSessionStorage<Trainer>(storageKey, trainer!);
    this._trainer = trainer;
  }

  //Getting session storage and updating trainer state.
  constructor() {
    this._trainer =
      sessionStorageUtil.getFromSessionStorage<Trainer>(storageKey);
  }
}
