import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing-routing.module';

import { AppComponent } from './app.component';
import { LoginView } from './views/login/login.view';
import { PokemonCatalogueView } from './views/pokemon-catalogue/pokemon-catalogue.view';
import { TrainerView } from './views/trainer/trainer.view';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { TrainerListComponent } from './components/trainer-list/trainer-list.component';
import { TrainerListItemComponent } from './components/trainer-list-item/trainer-list-item.component';
import { PokemonItemButtonComponent } from './components/pokemon-item-button/pokemon-item-button.component';
import { ShowMoreComponent } from './components/show-more/show-more.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LogoutButtonComponent } from './components/logout-button/logout-button.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginView,
    PokemonCatalogueView,
    TrainerView,
    LoginFormComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    TrainerListComponent,
    TrainerListItemComponent,
    PokemonItemButtonComponent,
    ShowMoreComponent,
    NavbarComponent,
    LogoutButtonComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
