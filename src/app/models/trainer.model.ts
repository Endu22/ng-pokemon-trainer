export type Trainer = {
    id: number;
    username: string;
    pokemon: string[];
}