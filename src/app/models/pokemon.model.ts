export interface Pokemon {
  name: string;
  url: string;
}
export type pokemonNameAndUrl = {
  count: number,
  next: string,
  previous: any,
  results: Pokemon[]
}


export interface extraPokeData {
  abilities: []
  stats: []
}