import { Component, Input, OnInit } from '@angular/core';
import { storageKey } from 'src/app/consts/storage-keys';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';
import { sessionStorageUtil } from '../../utils/session-storage.util';
@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.view.html',
  styleUrls: ['./trainer.view.css'],
})
export class TrainerView implements OnInit {
  get trainer(): Trainer {
    return this.trainerService.trainer!;
  }

  constructor(private readonly trainerService: TrainerService) {}

  ngOnInit(): void {}
}
