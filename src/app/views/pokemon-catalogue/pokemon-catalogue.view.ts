import { Component, OnInit } from '@angular/core';
import { pokemonKey } from 'src/app/consts/storage-keys';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { PokemonService } from 'src/app/services/pokemon.service';
import { LocalStorageUtil } from 'src/app/utils/session-storage.util';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.view.html',
  styleUrls: ['./pokemon-catalogue.view.css'],
})
export class PokemonCatalogueView implements OnInit {
  get pokemons(): Pokemon[] {
    return this.pokeCatalogueService.pokemons;
  }

  constructor(
    private readonly pokemonService: PokemonService,
    private readonly pokeCatalogueService: PokemonCatalogueService
  ) {}

  ngOnInit(): void {
    // If there is no data in localStorage -> fetch all pokemon.
    // Else, fetch pokemons from localStorage and set state in pokeCatalogueService.
    if (LocalStorageUtil.getLocalStorage(pokemonKey) === undefined) {
      this.pokemonService.getPokemon().subscribe((pokemonRes) => {
        // Store Pokemon[] in localStorage.
        LocalStorageUtil.saveToLocalStorage(pokemonKey, pokemonRes.results);
        // Set state in pokeCatalogueService to received Pokemon[]
        this.pokeCatalogueService.pokemons = pokemonRes.results;
      });
    } else {
      this.pokeCatalogueService.pokemons =
        LocalStorageUtil.getLocalStorage<Pokemon[]>(pokemonKey)!;
    }
  }
}
