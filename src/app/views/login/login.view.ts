import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.view.html',
  styleUrls: ['./login.view.css'],
})
export class LoginView implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly trainerService: TrainerService
  ) {}

  ngOnInit(): void {
    if (this.trainerService.trainer !== undefined) {
      this.router.navigateByUrl('/pokemon-catalogue');
    }
  }

  redirectAfterAuth(): void {
    this.router.navigateByUrl('/pokemon-catalogue');
  }
}
