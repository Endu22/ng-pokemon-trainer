import { environment } from 'src/environments/environment';

const { apiKey } = environment;

export const headers = {
  'Content-Type': 'application/json',
  'x-api-key': apiKey,
};
