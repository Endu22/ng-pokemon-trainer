# Pokemon Angular Application

## Contributors

Daniel Bengtsson - daniel.johan.gunnar.bengtsson@se.experis.com & Oliver Rimmi - oliver.rimmi@se.experis.com

## Usage

To get this project working you will have to put the correct environment keys in `src/environments/environment.ts` & `src/environments/environment.prod.ts`

1. Open `src/environments/environment.ts` and fill `''` in `apiTrainerUrl: '',` with the url given in moodle.
2. Open `src/environments/environment.ts` and fill `''` in `apiKey: '',` with the key given in moodle.
3. Open `src/environments/environment.ts` and fill `''` in `pokemonUrl: '',` with the url given in moodle.
4. Open `src/environments/environment.prod.ts` and repeat step `1-3`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.
